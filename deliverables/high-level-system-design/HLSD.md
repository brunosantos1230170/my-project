
High-Level System Diagram:

![HLSD](HLSD.png)

## ClinicalAPP Frontend (Presentation Layer)

The frontend of the Clinic Center Management System serves as the primary interface for patients, doctors, receptionists, and administrators. Developed using React, a leading JavaScript library for user interfaces, the frontend ensures a dynamic and responsive experience. Its key responsibilities include:

- **Displaying Information:** The system presents essential data such as patient lists, appointment specifics, medical records, invoices, and more, tailored to the roles of the logged-in users.
- **User Input:** It facilitates user interactions, allowing for authentication, scheduling appointments, updating medical histories, creating invoices, and account management.
- **Interaction with Backend:** The frontend interacts with the backend solely through API calls; it does not access the database directly. This layer adapts its display and functionalities based on the data and responses received from the backend.

## Backend

Acting as the core processing center of the application, the backend manages data flow between the frontend and the database. Utilizing Spring Boot 3, the backend is structured into several key components:

- **API Layer:** This layer offers well-defined endpoints (URLs) for the frontend to invoke actions and retrieve data, ensuring a clear interface for client-server communication.
- **Business Logic:** Implements the application's essential functions, such as verifying doctor availability and handling bookings when a patient schedules an appointment. It updates the frontend accordingly to reflect these changes.
- **Authentication:** Utilizes JSON Web Tokens (JWT) to authenticate users. During login, users submit their credentials, leading to the issuance of a JWT upon verification. This token, embedded in subsequent requests, enables secure and efficient user identification and authorization across sessions.
- **Data Access:** Manages interactions with the database, facilitating data retrieval, updates, and storage. This layer ensures that all data transactions are secure and efficient.

## Database

The system's relational database, powered by MySQL, is the central repository for all application data, responsible for:

- **User Data:** Stores comprehensive details about all user categories within the system, including patients, doctors, and administrative staff, encompassing IDs, roles, names, and authentication data.
- **Clinic Data:** Maintains records related to clinic operations such as appointments and financial transactions (invoices).

## Disk Storage

The disk storage component is dedicated to preserving sensitive and large data sets, such as detailed patient exams. This ensures data durability and accessibility while maintaining compliance with data protection regulations.
