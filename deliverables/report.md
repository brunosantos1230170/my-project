
# Clinic Center Management System

## Overview

The Clinic Center Management System is a web-based application developed using JHipster, Java for the back-end, and React for the front-end. This system aims to streamline the operations of a medical clinic center by providing a centralized platform for managing patient records, appointments, staff scheduling, and administrative tasks.

### Key Features
1. **User Authentication and Authorization:**
   - Different roles will be implemented including Admin, Doctor, Receptionist and Patient.
   - Admin will have full access to all features, while other roles will have access based on their permissions.

2. **Patient Management:**
   - Registration and profile management for patients.
   - Ability to view and update patient demographics and medical history.

3. **Appointment Scheduling:**
   - Patients can schedule appointments with available doctors.
   - Staff can manage appointment slots, confirm or reschedule appointments.

4. **Doctor and Staff Management:**
   - Admin can add, remove, or update doctor and staff profiles.
   - Assigning roles and specialties to doctors and staff members.

5. **Medical Records Management:**
   - Secure storage and retrieval of patient medical records.
   - Ability for doctors to create, update, and view patient exams results.

6. **Billing and Invoicing:**
   - Ability to include invoices for consultations, procedures, and services.

## Roles and Responsibilities
1. **Admin:**
   - Manage users, roles, and permissions.
   - View every patient data

2. **Doctor:**
   - View patient appointments and medical records, invoices.
   - Provide diagnoses, treatments, and prescriptions.

3. **Patient:**
   - View and update personal information and medical history.
   - Schedule appointments with available doctors.
   - Access medical records, test results and invoicings.

The Clinic Center Management System will enable seamless operations within the clinic center, facilitating efficient patient care, staff collaboration, and administrative management. Users will be able to access the system through a user-friendly interface, perform their respective tasks based on their roles and permissions, and contribute to the overall efficiency and effectiveness of the clinic center's operations. From patient registration to appointment scheduling, medical treatment, and billing, the system will streamline processes, enhance communication, and improve patient outcomes.

## High Level Architecture

The high-level system diagram is as follows:

![HLSD](high-level-system-design/HLSD.png)

### ClinicalAPP Frontend (Presentation Layer)

The frontend of the Clinic Center Management System serves as the primary interface for patients, doctors, receptionists, and administrators. Developed using React, a leading JavaScript library for user interfaces, the frontend ensures a dynamic and responsive experience. Its key responsibilities include:

- **Displaying Information:** The system presents essential data such as patient lists, appointment specifics, medical records, invoices, and more, tailored to the roles of the logged-in users.
- **User Input:** It facilitates user interactions, allowing for authentication, scheduling appointments, updating medical histories, creating invoices, and account management.
- **Interaction with Backend:** The frontend interacts with the backend solely through API calls; it does not access the database directly. This layer adapts its display and functionalities based on the data and responses received from the backend.

### Backend

Acting as the core processing center of the application, the backend manages data flow between the frontend and the database. Utilizing Spring Boot 3, the backend is structured into several key components:

- **API Layer:** This layer offers well-defined endpoints (URLs) for the frontend to invoke actions and retrieve data, ensuring a clear interface for client-server communication.
- **Business Logic:** Implements the application's essential functions, such as verifying doctor availability and handling bookings when a patient schedules an appointment. It updates the frontend accordingly to reflect these changes.
- **Authentication:** Utilizes JSON Web Tokens (JWT) to authenticate users. During login, users submit their credentials, leading to the issuance of a JWT upon verification. This token, embedded in subsequent requests, enables secure and efficient user identification and authorization across sessions.
- **Data Access:** Manages interactions with the database, facilitating data retrieval, updates, and storage. This layer ensures that all data transactions are secure and efficient.

### Database

The system's relational database, powered by MySQL, is the central repository for all application data, responsible for:

- **User Data:** Stores comprehensive details about all user categories within the system, including patients, doctors, and administrative staff, encompassing IDs, roles, names, and authentication data.
- **Clinic Data:** Maintains records related to clinic operations such as appointments and financial transactions (invoices).

### Disk Storage

The disk storage component is dedicated to preserving sensitive and large data sets, such as detailed patient exams. This ensures data durability and accessibility while maintaining compliance with data protection regulations.

## Deployment Diagram 

![alt text](deployment-diagram/DeploymentDiagram.png)

The present deployment diagram depicts a web-based Clinic Center Management System deployment using JHipster. 

Here's a breakdown of the components:

 - **Database Server**: This device runs the MySQL database, which likely stores the clinic's patient information, appointments, medical records, and potentially other data.
 - **JHipster Application**: This artifact, deployed on the application server, represents the web application itself. JHipster is a framework used to generate a backend and a frontend for web applications.
 - **Application Server**: This device is where the web application runs. It likely contains a Java runtime environment and potentially other software required to run the JHipster application.
    - **React Application**: JHipster generates a single-page application (SPA) frontend using the React JavaScript library. This frontend handles user interactions, manages the user interface (UI), and communicates with the backend API.
    - **Spring Backend**: JHipster also  generates a Spring Boot backend application written in Java. This backend API processes user requests received from the React frontend, interacts with the database using JDBC, and provides the necessary data and functionalities for the application.
 - **Client**: This device represents the user's computer or mobile device that runs a web browser to interact with the Clinic Center Management System web application.
 - **Browser**: This artifact represents the software used to access the web application, such as Google Chrome, Safari, or Mozilla Firefox.
 - **HTML**: This artifact represents the web pages users see in their browser. The JHipster application likely generates these HTML pages dynamically based on user interaction and data retrieved from the database.

The connections present in this deployment diagram are:

 - **Web Browser -> (HTTPS) -> Application Server**: This connection signifies user interaction with the system. Arrows indicate the direction of communication, and "HTTPS" specifies a secure communication protocol using encryption. Users on their web browsers send requests (e.g., login attempts, appointment bookings) to the application server.

 - **Application Server -> (JDBC) -> Database Server**: This connection represents data access between the application server and the database. "JDBC" indicates Java Database Connectivity, a technology the JHipster application likely uses to interact with the MySQL database. The application server retrieves or stores data in the database based on user requests.

Overall, the deployment diagram illustrates a typical three-tier architecture for a web application:

 - **Presentation Layer (Client)**: This layer consists of the user's device, web browser, and the HTML content displayed.
 - **Business Logic Layer (Application Server)**: This layer houses the JHipster application, which processes user interactions and fetches/stores data from the database.
 - **Data Layer (Database Server)**: This layer stores the application's data in a MySQL database.

In this deployment scenario, the Clinic Center Management System web application would be accessed through a web browser on any device with an internet connection. This would allow for remote access, potentially by staff or patients, from anywhere.

## Features Description

### Feature 1: Login / Register

#### DFD

![login-register-dfd](dfd/register-login.png)

#### Abuse cases

Based on the Data Flow Diagram (DFD) for the login and registration feature of our application, we have identified key abuse cases and associated vulnerabilities:


- **Credential Theft:**
An attacker could gain unauthorized access to user accounts by stealing login credentials. This occurs through methods such as phishing attacks, credential stuffing using details from other breaches, or through keylogging malware.

- **Account Creation Fraud:**
Malicious actors exploit the registration process to create fraudulent accounts. These accounts can be used for spamming, phishing attacks from seemingly legitimate accounts, or disrupting service with fake sign-ups.

- **Injection Attacks:**
Attackers could exploit vulnerabilities in form input handling to inject malicious code. This includes SQL Injection in login or registration forms, which could lead to unauthorized access to database information.

- **Cross-Site Scripting (XSS):**
During the login or registration process, attackers might inject scripts into input fields. These scripts can execute malicious actions in the browsers of other users, compromising client-side security.

Ways of solving these issues:

- **Strong Authentication Mechanisms:**
Enforce multi-factor authentication (MFA) during login to add an extra layer of security beyond just username and password.

- **Input Validation and Sanitization:**
Implement robust validation and sanitization of user inputs to prevent SQL injection and XSS attacks. All inputs should be checked against a strict set of rules.

- **Rate Limiting and CAPTCHA:**
Use rate limiting to prevent automated attacks such as credential stuffing or brute force attacks. Implement CAPTCHA mechanisms to distinguish human users from bots during both login and registration processes.

- **Account Verification:**
Require email or phone verification during the account registration process to validate the authenticity of new users.

- **Encryption and Secure Storage:**
Use strong encryption methods for storing sensitive user information and credentials. Ensure that data transmitted during the login and registration processes is encrypted using secure protocols like HTTPS.


By incorporating these strategies, the application can better safeguard against abuse cases associated with the login and registration features, thus enhancing overall system security.

#### Attack Tree

- **Login:**
![Login-attack-tree](attack-tree/login-attack-tree.png)

- **Registration:**
![Register-attack-tree](attack-tree/register-attack-tree.png)

#### Sequence diagram

- **Login sequence diagram:**
![Login-sequence-diagram](sequence-diagram/login-sd.png)

- **Registration sequence diagram:**
![Register-sequence-diagram](sequence-diagram/register-sd.png)

### Feature 2: Manage personal data

#### DFD

![Manage-personal-data](dfd/manage-personal-data.png)

#### Abuse cases

Based on the patient's personal data DFD, we've identified two possible abuse cases:

- **Unauthorized Access:** An attacker could gain unauthorized access to patients' personal data by exploiting some vulnerabilities in the system. This could be done through phising (emails), social engineering, or other ways.

- **Data Tampering:** An authorized user, such as a high-level user, could tamper with a patients' personal data. In this case, data tampering could cause as much as incorrect medical treatment.

Ways of solving this issues:

- **Authentication and authorization:** Users will be required to authenticate themselves before they can access any data. Once authenticated, each user will only be able to access data that his permissions allow him to access.

- **Encryption:** Data in Rest and also important/confidential data store onto the database should be encrypted. This will help to prevent plain access to data from unauthorized access, even if an attacker is able to gain access to the system.

- **Logging:** All data access should be logged so, in advance, malicious activity can be more easily detected.

By implementing these means, it can help to mitigate the risk of abuse cases and protect patients' data.

#### Attack Tree

Steal Patient Data
     |
     +-- Gain Unauthorized Access to System
     |       |
     |       +-- Steal Login Credentials (Social Engineering, Phishing)
     |       +-- Exploit Vulnerability (Buffer Overflow, SQL Injection)
     |
     +-- Tamper with Data in Transit (Man-in-the-Middle Attack)
     |
     +-- Intercept Data at Rest (Unencrypted Storage)

#### Sequence diagram

![Manage-personal-data-sequence-diagram](sequence-diagram/manage-personal-data.png)

### Feature 3: Manage users

#### DFD

![Manage-users](dfd/manage-users.png)

#### Abuse cases

Based on the patient's personal data DFD, we've identified three possible abuse cases:

- **Tampering with User Input:** The malicious user attempts to manipulate system behavior by injecting unexpected code or data into user input fields.

- **Exploiting System Vulnerabilities:** The malicious user takes advantage of weaknesses in the system's code or configuration to gain unauthorized access or manipulate user data.

- **Interception of Data Flows:** The malicious user intercepts data transmissions between the user and the system to steal sensitive information or manipulate data in transit.

Ways of solving this issues:

- **Tampering with User Input:** Implement strong input validation to ensure user inputs according to expected result and doesn't contain malicious code.

- **Exploiting System Vulnerabilities:** Conduct vulnerability assessments and penetration testing to identify and fix weaknesses. Implement security best practices in system development and configuration.

- **Interception of Data Flows:** Implement data encryption for all communication between users and the system (e.g., HTTPS).
Use secure network protocols and configurations to prevent unauthorized access to data traffic.
Monitor network activity for suspicious behavior that might indicate interception attempts.

By implementing these means, it can help to mitigate the risk of abuse cases and protect users' data.


#### Attack Tree

Unauthorized access or manipulation
  |
  +-- Tampering with User Input
  |     |
  |     +-- SQL Injection
  |     +-- Cross-Site Scripting (XSS)
  |
  +-- Exploiting System Vulnerabilities
  |     |
  |     +-- Unpatched Software
  |     +-- Buffer Overflow Attacks
  |     +-- Privilege Escalation
  |
  +-- Interception of Data Flows
        |
        +-- Man-in-the-Middle Attacks
        +-- Sniffing Network 


### Feature 4: Schedule appointment

#### DFD

![Schedule-appointment](dfd/schedule-appointment.png)

#### Abuse cases

Based on the schedule appointment DFD, we've identified four possible abuse cases:

- **Appointment Hoarding:** A malicious user creates a large number of fake appointments to block legitimate users from booking their own.

- **Impersonation and Identity Theft:** This is when an attacker steals a legitimate user's credentials and perform wrong tasks in the system like schedule unwanted appointments or even canceling, or reschedule, appointments in order to disrupt healthcare delivery.

- **Data Leakage:** An unauthorized user gains access to the appointment system and steals sensitive patient data, including names, contact information, and potentially medical details.

- **Denial-of-Service (DoS) Attack:** An attacker floods the appointment system with requests, overwhelming the system and preventing legitimate users from booking appointments.

Ways of solving this issues:

- **Appointment Hoarding:** Implement CAPTCHAs or other verification methods during appointment booking to prevent automated scripts. Consider limiting the number of appointments a single user can book within a specific timeframe.

- **Impersonation and Identity Theft:** Enforce strong password policies and two-factor authentication for user logins. Implement processes to verify user identity during suspicious activity like, per example, booking multiple appointments in a short period.

- **Data Leakage:** Encrypt all sensitive data at rest and in transit. Implement access controls to restrict access to appointment data based on user roles and needs. Regularly monitor system activity for suspicious behavior.

- **Denial-of-Service (DoS) Attack:** Implement security measures to identify and block DoS attacks. Consider using rate limiting to prevent excessive requests from a single source.

By implementing these means, it can help to mitigate the risk of abuse cases and protect appointments' data.


#### Attack Tree

Steal Appointment Data
     |
     +-- Gain Unauthorized Access to System
     |       |
     |       +-- Steal Login Credentials (Social Engineering, Phishing)
     |       |
     |       +-- Target Staff with Appointment Access (DFD User Access)
     +-- Exploit Vulnerability (Buffer Overflow, SQL Injection)
     |
     +-- Tamper with Data in Transit (Man-in-the-Middle Attack)
     |       |
     |       +-- Interfere with Network Traffic During Scheduling
     |
     +-- Intercept Data at Rest (Unencrypted Storage)
     |
     +-- Unencrypted Appointment Database


#### Sequence diagram

![Schedule-appointment-sequence-diagram](sequence-diagram/schedule-appointment.png)

### Feature 5: Manage appointments

#### DFD

![manage-appointments.png](dfd%2Fmanage-appointments.png)

#### Abuse cases

## Abuse-case for manage appointments

### Sensitive Data Manipulation:
**Abuse Case:** Hackers may try to access and manipulate sensitive patient data, such as medical history, for various harmful purposes, such as blackmail or identity theft.

**Mitigation**: Create regular and secure backups of the system's data to ensure recovery in case of data manipulation or loss.


### Phishing and Social Engineering Attacks:
**Abuse Case:** Attackers may try to obtain make phishing with users with fake emails and messages for appointment confirmation for example.

**Mitigation:** Never send links and ask customers to click buttons in the emails. Always inform and guide the user to access the system directly.

#### Attack Tree

![manage-appointments.png](attack-tree%2Fmanage-appointments.png)

#### Sequence diagram

![manage-appointments.png](sequence-diagram%2Fmanage-appointments.png)

### Feature 6: Documents publishing

#### DFD

### Upload file
![upload-file.png](sequence-diagram%2Fupload-file.png)

### Download file
![download-file.png](sequence-diagram%2Fdownload-file.png)

#### Abuse cases

## Abuse-case for documents publishing

### Unauthorized Access to Documents:
**Abuse Case:** A malicious actor may attempt to access documents stored in the server disk without authorization, exploiting vulnerabilities in the authentication system or conducting brute-force attacks to gain unauthorized access to the documents.
This could result in compromised privacy and security of patient data, as well as potential violations of data protection regulations.

**Mitigation**: Implement strong authentication, such as two-factor authentication (already covered in the login feature), to ensure that only authorized users can access the documents.
Additionally, apply appropriate access control policies to restrict access only to authorized users and ensure that has right access.

### Leakage of Personal Data:
**Abuse Case:** If the system is not properly secured, leakage of patients' personal information contained in the documents stored in the server disk can occur.
This could result in significant privacy violations and damage to the clinic's reputation, as well as potential legal and financial implications, especially if it is in violation of regulations such as GDPR.

**Mitigation:** Encrypt the data stored in the server to protect against data leaks in case of unauthorized access.
Additionally, implement additional security measures such as data anonymization whenever possible and data security practices compliant with data protection regulations such as GDPR.

### Invalid File Paths or Execution Attempts:
**Abuse Case:** Malicious actors may attempt to pass invalid file paths or execute malicious code by exploiting vulnerabilities in the system. This could lead to system instability, data corruption, or unauthorized access to sensitive information.

**Solution:** Implement input validation and sanitization mechanisms to ensure that file paths are valid and do not pose security risks. Additionally, employ security measures such as file type verification and restrict file execution privileges to prevent unauthorized code execution.

### File Size and Extension Issues:
**Abuse Case:** Users may attempt to upload excessively large files or files with potentially harmful extensions, such as executable files or scripts, which could consume system resources, cause system instability, or compromise security.

**Solution:** Implement file size restrictions to prevent the upload of excessively large files that could overwhelm system resources. Additionally, enforce restrictions on file extensions and employ file type verification mechanisms to ensure that only safe file types are allowed to be uploaded.

#### Attack Tree

![documents-publishing-and-access.png](attack-tree%2Fdocuments-publishing-and-access.png)

#### Sequence diagram

### Upload file
![upload-file.png](sequence-diagram%2Fupload-file.png)Sequence diagram file/description

### Upload file
![download-file.png](sequence-diagram%2Fdownload-file.png)

## Risk Matrix Analysis for Clinic Center Management System

For the creation of the risk matrix analysis for the Clinic Center Management System, the following standard risk matrix was utilized:

![alt text](risk-matrix/StandardRiskMatrix.png)

</br>

The below table outlines a risk matrix analysis for key features of the Clinic Center Management System. The likelihood and consequence are rated based on a scale of 1 (Rare/Negligible) to 5 (Almost Certain/Catastrophic). The final risk level is calculated by multiplying the likelihood and consequence ratings.

</br>


**Risk Analysis by Feature Type (with Risk Level):**

| Feature Type | Risk | Description | Mitigation Strategies |
|---|---|---|---|
| Data Access & Management (20) | Unauthorized data access (breaches, attacks) | Patient data breaches due to vulnerabilities in access controls or security practices. | Implement robust access controls, data encryption at rest and in transit, conduct regular security audits. |
| User Authentication & Authorization (16) | Weak authentication leading to account compromise | Weak passwords or authentication methods could allow unauthorized access to user accounts. | Enforce strong authentication methods (e.g., multi-factor authentication), require complex passwords with periodic updates. |
| Appointment Scheduling & Management (6) | System bugs causing booking issues or data loss | System bugs or errors could lead to double bookings, scheduling conflicts, or data loss. | Conduct thorough system testing to identify and fix bugs, implement data backups and a disaster recovery plan. |
| Document Management & Publishing (4) | Accidental publishing of incorrect/unauthorized documents | Accidental publishing of sensitive information due to human error or lack of access controls. | Implement access controls for document publishing, train staff on proper document handling procedures. |
| System Availability & Performance (2) | System outages or performance issues | System outages or performance issues can disrupt clinic operations and potentially lead to data loss. | Regularly monitor system health, implement redundancy measures (e.g., backups, failover), and have a disaster recovery plan in place. |

**Risk Level Calculation:**

The Risk Level is calculated by multiplying the likelihood rating by the consequence rating. A high-risk category indicates a greater need for mitigation strategies. 

**Note:** This analysis provides a general overview. Specific implementations may require adjustments based on your system's unique features and data sensitivity. 





