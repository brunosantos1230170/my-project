# Test Plan: Security Testing for ClinicalAPP System

## 1. Automated Security Scanning

### Static Application Security Testing (SAST):

**Objective:** Analyze source code at rest to identify security vulnerabilities.  
**Tools:** We will use SonarQube to automatically scan the React and Spring Boot codebases for common security issues.  
**Pipeline Integration:** The step will be integrated in the pipeline in a way that if there’s some vulnerability detected, the pipeline will fail and prevent the code from being deployed.

### Dynamic Application Security Testing (DAST):

**Objective:** Test the running application for vulnerabilities that an attacker could exploit.  
**Tools:** We will use OWASP ZAP to perform automated scans against deployed environments. It will do Interception and Modification of Traffic in order to check how the app will react to malicious input and will check for vulnerabilities such as SQL injection, cross-site scripting (XSS).  
**Pipeline Integration:** The step will be integrated in the pipeline in a way that if there's some vulnerability detected, the pipeline will fail and prevent the code from being deployed.

### Software Composition Analysis (SCA):

**Objective:** Identify known vulnerabilities in third-party libraries and dependencies.  
**Tools:** Utilize OWASP Dependency-Check to scan project dependencies.  
**Pipeline Integration:** The step will be integrated in the pipeline in a way that if there's some vulnerability on any dependency detected, the pipeline will fail and prevent the code from being deployed.

## 2. Authentication and Authorization Testing

### JWT Token Handling Testing

**Objective:** Ensure that JWT token handling mechanisms are robust and applied consistently during data access.

**Test Cases:**
- Verify that JWTs are properly signed and cannot be tampered with to prevent unauthorized access.
- Ensure tokens have an appropriate expiration set to minimize the risk of misuse over extended periods.
- Confirm that the tokens are securely stored and transmitted using industry-standard encryption techniques.

**Tools:** Employ integration tests and Postman for automated testing. Integration tests should validate the security and integrity of JWT handling by simulating real-world usage scenarios. Postman scripts can be used to systematically check that JWTs are correctly managed across all endpoints.

**Pipeline Integration:** This testing step will be integrated into the CI/CD pipeline to automatically execute JWT validation tests. If JWT handling fails any test, the pipeline will halt, preventing the deployment of potentially compromised code. This ensures the security of token mechanisms before any code is deployed to production.

### Authorization and Authentication Control Testing

**Objective:** Ensure that authentication and authorization protocols are strictly enforced across all endpoints to protect sensitive data and functionality.

**Test Cases:**
- Confirm that password policies enforce complexity requirements, reducing the risk of unauthorized access via brute force or weak passwords.
- Test role-based access control (RBAC) to ensure users can only access data and perform actions appropriate to their roles, maintaining strict access controls.
- Verify that sensitive data can only be accessed by authorized personnel.

**Tools:** Utilize integration tests and security auditing tools for these tests. Integration tests should replicate role-based access scenarios to ensure that authorization controls are properly enforced.

**Pipeline Integration:** This testing phase will be integrated into the CI/CD pipeline to run automatically. Any detection of security breaches will cause the pipeline to halt.

## 3. Data Security and Privacy Testing

### Encryption Validation for Sensitive Data:

**Objective:** Verify that all sensitive data, including user passwords and personal information, is encrypted and stored using industry-standard algorithms.  
**Tools:** We will use integration tests to test these aspects.

### Enforcement of Secure Communication Protocols:

**Objective:** HTTPS Enforcement. Check that all web endpoints redirect HTTP traffic to HTTPS and that the SSL/TLS configuration is up to date with strong ciphers.  
**Tools:** We will use the Spring SSL configuration to make sure that only HTTPS requests are accepted.

### Secure Headers and Cookies:

**Objective:** Implement and verify security-focused HTTP headers such as Content-Security-Policy, X-Frame-Options, and X-XSS-Protection to enhance the security posture of the application against common attack vectors and audit cookies to ensure that security attributes like HTTPSOnly and Secure are consistently implemented. Test for proper scope settings by specifying the Path and Domain attributes, and use the SameSite attribute to mitigate the risk of cross-site request forgery (CSRF).  
**Tools:** This point coincides with the DAST test, so the same tool will be used.

## 4. API Security Testing

**Objective:** Protect the backend from common API vulnerabilities.

**Test Cases:**
- Implement tests for SQL Injection on API endpoints.
- Test for Cross-Site Scripting (XSS) vulnerabilities in inputs that are reflected in API outputs.
- Verify that API rate limiting is effective against DDoS attacks.
- Check for proper implementation of CORS policies.

**Tools:** The SQL Injection and XSS vulnerabilities were already described in the previous points. The API limiting for the DDoS attacks used will be implemented using bucket4j, a standard spring tool to manage these settings. The check for proper CORS policies implementation will be done resorting to Postman tests.

**Pipeline Integration:** This testing phase will be integrated into the CI/CD pipeline to run automatically. Any detection of security breaches will cause the pipeline to halt.

## 5. Security Misconfiguration Testing

**Objective:** Ensure there are no security misconfigurations in the deployment environment.

**Test Cases:**
- Review and validate configuration settings for all servers and databases.
- Check for default, unchanged passwords or open ports.
- Ensure error messages are scrubbed of sensitive information that could be leveraged for further attacks.

**Tools:** In order to make sure that no passwords are committed to the git repository, we will use TruffleHog to find possible credentials being committed.

**Pipeline Integration:** This testing phase will be integrated into the CI/CD pipeline to run automatically. Any detection of security breaches will cause the pipeline step to fail but won't prevent the deployment of the app.
