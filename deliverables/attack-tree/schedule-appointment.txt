Steal Appointment Data
     |
     +-- Gain Unauthorized Access to System
     |       |
     |       +-- Steal Login Credentials (Social Engineering, Phishing)
     |       |
     |       +-- Target Staff with Appointment Access (DFD User Access)
     +-- Exploit Vulnerability (Buffer Overflow, SQL Injection)
     |
     +-- Tamper with Data in Transit (Man-in-the-Middle Attack)
     |       |
     |       +-- Interfere with Network Traffic During Scheduling
     |
     +-- Intercept Data at Rest (Unencrypted Storage)
     |
     +-- Unencrypted Appointment Database
