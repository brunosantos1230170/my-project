## Input validation and sanitization

The Spring Framework, by default, offers some basic protections against HTTP Parameter Pollution (HPP) attacks.

The Spring Framework has a systematic approach to handling HTTP requests. In the context of a Spring web application, it is common to use annotations such as @RequestParam, @PathVariable, @RequestBody, and others to bind user input data to method variables in controllers. Spring automatically handles repeated HTTP parameters when binding these data to objects, usually using the first received value and ignoring the subsequent ones or throwing an error, depending on the configuration. This can help mitigate some risks associated with parameter pollution attacks, but it is important to have a data validation and sanitization strategy in place.

Besides that, the inputs are validated in the backend using the @Valid API and a DTO that defines the attributes as follows:

![img.png](assets%2Fimg.png)

![img_1.png](assets%2Fimg_1.png)


For more sensitive fields, such as passwords, stricter rules have been applied to ensure both the application's security and the user's security by encouraging strong passwords.

Validation occurs both on the frontend, to enhance user experience and guide users, and on the backend, where security must be enforced since frontend fields can be easily bypassed, so backend validations are crucial to ensure data integrity and security.

 
![img_2.png](assets%2Fimg_2.png)

Frontend validation
![img_3.png](assets%2Fimg_3.png)

Backend Validation
![img_4.png](assets%2Fimg_4.png)
