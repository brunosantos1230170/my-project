Based on the patient's personal data DFD, we've identified two possible abuse cases:

- **Unauthorized Access:** An attacker could gain unauthorized access to patients' personal data by exploiting some vulnerabilities in the system. This could be done through phising (emails), social engineering, or other ways.

- **Data Tampering:** An authorized user, such as a high-level user, could tamper with a patients' personal data. In this case, data tampering could cause as much as incorrect medical treatment.

[TO REMOVE AND INCLUDE ON REPORT]
Ways of solving this issues:

- **Authentication and authorization:** Users will be required to authenticate themselves before they can access any data. Once authenticated, each user will only be able to access data that his permissions allow him to access.

- **Encryption:** Data in Rest and also important/confidential data store onto the database should be encrypted. This will help to prevent plain access to data from unauthorized access, even if an attacker is able to gain access to the system.

- **Logging:** All data access should be logged so, in advance, malicious activity can be more easily detected.

By implementing these means, it can help to mitigate the risk of abuse cases and protect patients' data.
