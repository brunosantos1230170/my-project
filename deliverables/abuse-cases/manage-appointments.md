## Abuse-case for manage appointments

### Sensitive Data Manipulation:
**Abuse Case:** Hackers may try to access and manipulate sensitive patient data, such as medical history, for various harmful purposes, such as blackmail or identity theft.

**Mitigation**: Create regular and secure backups of the system's data to ensure recovery in case of data manipulation or loss.


### Phishing and Social Engineering Attacks:
**Abuse Case:** Attackers may try to obtain make phishing with users with fake emails and messages for appointment confirmation for example.

**Mitigation:** Never send links and ask customers to click buttons in the emails. Always inform and guide the user to access the system directly.